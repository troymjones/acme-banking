import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { EnvConfig } from '../modules/env/env.module'

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  constructor(private env: EnvConfig) {}

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.env.apiBaseUrl && this.env.apiBasePath && req.url.startsWith(this.env.apiBasePath)) {
      return next.handle(
        req.clone({
          url: this.env.apiBaseUrl + req.url,
        }),
      )
    }
    return next.handle(req)
  }
}

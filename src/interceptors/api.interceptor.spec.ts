import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { inject, TestBed } from '@angular/core/testing'
import { EnvConfig, EnvConfigDefault } from 'src/modules/env/env.module'
import { ApiInterceptor } from './api.interceptor'

describe('ApiInterceptor', () => {
  const testApiBaseUrl = 'http://my-test-base.com'
  const testApiBasePath = '/api'
  const testApiPath = `${testApiBasePath}/v1/customer/1`

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ApiInterceptor,
          multi: true,
        },
        { provide: EnvConfig, useValue: EnvConfigDefault },
      ],
    })
  })

  it('should not add apiBaseUrl if empty', inject(
    [HttpClient, HttpTestingController, EnvConfig],
    (http: HttpClient, mock: HttpTestingController, env: EnvConfig) => {
      env.apiBaseUrl = ''
      env.apiBasePath = testApiBasePath

      http.get(testApiPath).subscribe((res) => expect(res).toBeTruthy())
      const request = mock.expectOne(testApiPath)

      request.flush({})
      mock.verify()
    },
  ))

  it('should not add apiBaseUrl if apiBasePath is not found in request', inject(
    [HttpClient, HttpTestingController, EnvConfig],
    (http: HttpClient, mock: HttpTestingController, env: EnvConfig) => {
      env.apiBaseUrl = testApiBaseUrl
      env.apiBasePath = testApiBasePath
      const testPath = 'assets/i18n/en.json'

      http.get(testPath).subscribe((res) => expect(res).toBeTruthy())
      const request = mock.expectOne(testPath)

      request.flush({})
      mock.verify()
    },
  ))

  it('should add apiBaseUrl if apiBasePath is found', inject(
    [HttpClient, HttpTestingController, EnvConfig],
    (http: HttpClient, mock: HttpTestingController, env: EnvConfig) => {
      env.apiBaseUrl = testApiBaseUrl
      env.apiBasePath = testApiBasePath

      http.get(testApiPath).subscribe((res) => expect(res).toBeTruthy())
      const request = mock.expectOne(`${testApiBaseUrl}${testApiPath}`)

      request.flush({})
      mock.verify()
    },
  ))
})

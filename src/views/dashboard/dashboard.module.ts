import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { TranslateModule } from '@ngx-translate/core'
import { Dashboard } from './dashboard'
import { AccountListModule } from 'src/components/account-list/account-list.module'

@NgModule({
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    TranslateModule,
    AccountListModule,
    RouterModule.forChild([{ path: '', component: Dashboard }]),
  ],
  declarations: [Dashboard],
  exports: [Dashboard],
})
export class DashboardModule {}

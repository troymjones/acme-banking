import { Component, OnInit } from '@angular/core'
import { Customer } from 'src/models/customer'
import { UntilDestroy } from '@ngneat/until-destroy'
import { Observable } from 'rxjs'
import { CustomerService } from 'src/services/customer.service'
import { AcmeSessionService } from 'src/services/acme-session.service'
import { DateTime } from 'luxon'
import _ from 'lodash-es'

@UntilDestroy()
@Component({
  selector: 'dashboard',
  templateUrl: 'dashboard.html',
  styleUrls: ['dashboard.scss'],
})
export class Dashboard implements OnInit {
  public customer$: Observable<Customer>

  private readonly customerId: string

  constructor(private customerService: CustomerService, private sessionService: AcmeSessionService) {
    this.customerId = this.sessionService.getCustomerId()
  }

  public ngOnInit(): void {
    this.getCustomerDetails()
  }

  public getCustomerDetails(): void {
    this.customer$ = this.customerService.getCustomer(this.customerId)
  }

  public getFormattedMonthYear(date: string): string {
    return DateTime.fromISO(date).toFormat('MMMM yyyy')
  }
}

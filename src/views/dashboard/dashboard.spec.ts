import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { of } from 'rxjs'
import _ from 'lodash-es'
import { RouterTestingModule } from '@angular/router/testing'
import { instance, when } from 'testdouble'
import { Dashboard } from './dashboard'
import { AccountService } from 'src/services/account.service'
import { CustomerService } from 'src/services/customer.service'
import { AcmeSessionService } from 'src/services/acme-session.service'
import { Customer } from 'src/models/customer'
import { Account } from 'src/models/account'
import { TranslateTestModule } from 'src/modules/translate/translate.mock'

describe('Dashboard', () => {
  let component: Dashboard
  let fixture: ComponentFixture<Dashboard>
  let customerService: CustomerService
  let accountService: AccountService
  let sessionService: AcmeSessionService

  const testCustomerId = '1'

  const testCustomer: Customer = {
    id: testCustomerId,
    avatar: 'my-avatar.png',
    createdAt: '2021-01-13T06:32:51.868Z',
    customerId: '398483',
    name: 'John Doe',
  }

  const testAccounts: Account[] = [
    {
      id: '1',
      customerId: testCustomerId,
      createdAt: '2021-01-13T06:32:51.868Z',
      accountNumber: '81638173',
      accountName: 'Money Market Account',
      balance: '433.41',
    },
    {
      id: '2',
      customerId: testCustomerId,
      createdAt: '2021-01-13T06:32:51.868Z',
      accountNumber: '81638173',
      accountName: 'Money Market Account',
      balance: '433.41',
    },
  ]

  beforeEach(
    waitForAsync(() => {
      customerService = instance<CustomerService>(CustomerService)
      when(customerService.getCustomer(testCustomerId)).thenReturn(of(testCustomer))

      accountService = instance<AccountService>(AccountService)
      when(accountService.getAccounts(testCustomerId)).thenReturn(of(testAccounts))

      sessionService = instance<AcmeSessionService>(AcmeSessionService)
      when(sessionService.getCustomerId()).thenReturn(testCustomerId)

      try {
        TestBed.configureTestingModule({
          schemas: [NO_ERRORS_SCHEMA],
          declarations: [Dashboard],
          imports: [BrowserAnimationsModule, RouterTestingModule, TranslateTestModule],
          providers: [
            {
              provide: CustomerService,
              useValue: customerService,
            },
            {
              provide: AcmeSessionService,
              useValue: sessionService,
            },
            {
              provide: ActivatedRoute,
              useValue: { params: of({ id: '1' }) },
            },
          ],
        }).compileComponents()
      } catch (error) {
        console.error(error)
      }
    }),
  ),
    beforeEach(() => {
      fixture = TestBed.createComponent(Dashboard)
      component = fixture.componentInstance
      fixture.detectChanges()
    })

  it('should initialize Dashboard successfully', (done) => {
    component.customer$.subscribe((customer) => {
      expect(customer).toEqual(testCustomer)
      done()
    })
  })

  it('should get formatted month/year correctly', () => {
    expect(component.getFormattedMonthYear('2021-01-02')).toEqual('January 2021')
  })
})

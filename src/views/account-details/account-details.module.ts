import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { AccountService } from 'src/services/account.service'
import { TransactionService } from 'src/services/transaction.service'
import { AccountDetails } from './account-details'
import { AccountListModule } from 'src/components/account-list/account-list.module'
import { AccountSelectedModule } from 'src/components/account-selected/account-selected.module'

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    AccountListModule,
    AccountSelectedModule,
    RouterModule.forChild([{ path: ':id', component: AccountDetails }]),
  ],
  declarations: [AccountDetails],
  providers: [AccountService, TransactionService],
})
export class AccountDetailsModule {}

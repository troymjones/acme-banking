import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { Observable } from 'rxjs'
import { map, switchMap } from 'rxjs/operators'
import { AccountService } from 'src/services/account.service'
import _ from 'lodash-es'
import { Transaction, TransactionType } from 'src/models/transaction'
import { Account } from 'src/models/account'
import { AcmeSessionService } from 'src/services/acme-session.service'
import { TransactionService } from 'src/services/transaction.service'
import { TranslateService } from '@ngx-translate/core'

@UntilDestroy()
@Component({
  selector: 'account-details',
  templateUrl: 'account-details.html',
  styleUrls: ['account-details.scss'],
})
export class AccountDetails implements OnInit {
  public accounts$: Observable<Account[]>
  public account$: Observable<Account>
  public transactions$: Observable<Transaction[]>

  public selectedAccountId: string

  private readonly customerId: string

  constructor(
    private accountService: AccountService,
    private transactionService: TransactionService,
    private translate: TranslateService,
    private session: AcmeSessionService,
    private route: ActivatedRoute,
  ) {
    this.customerId = this.session.getCustomerId()
  }

  public ngOnInit(): void {
    this.accounts$ = this.accountService.getAccounts(this.customerId)
    this.getAccount()
  }

  public getAccount(): void {
    setTimeout(() => {
      this.account$ = this.route.params.pipe(
        untilDestroyed(this),
        map((params: Params) => _.get(params, 'id')),
        switchMap((id: string) => {
          setTimeout(() => {
            this.selectedAccountId = id
            this.getTransactions(id)
          })
          return this.accountService.getAccount(this.customerId, id)
        }),
      )
    })
  }

  public getTransactions(accountId: string): void {
    this.transactions$ = this.transactionService.getTransactions(this.customerId, accountId)
  }

  public formatTransactionType(type: TransactionType): string {
    return this.translate.instant('transaction.type.' + type)
  }
}

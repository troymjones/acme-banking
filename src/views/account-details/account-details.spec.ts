import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { of } from 'rxjs'
import _ from 'lodash-es'
import { AccountService } from 'src/services/account.service'
import { TransactionService } from 'src/services/transaction.service'
import { RouterTestingModule } from '@angular/router/testing'
import { instance, when } from 'testdouble'
import { AccountDetails } from './account-details'
import { Transaction, TransactionType } from 'src/models/transaction'
import { Account } from 'src/models/account'
import { AcmeSessionService } from 'src/services/acme-session.service'
import { TranslateTestModule } from 'src/modules/translate/translate.mock'

describe('AccountDetails', () => {
  let component: AccountDetails
  let fixture: ComponentFixture<AccountDetails>
  let accountService: AccountService
  let transactionService: TransactionService
  let sessionService: AcmeSessionService

  const testCustomerId = '111'
  const testAccountId = '555'

  const testAccount: Account = {
    id: testAccountId,
    customerId: testCustomerId,
    createdAt: '2021-01-13T06:32:51.868Z',
    accountNumber: '81638173',
    accountName: 'Money Market Account',
    balance: '433.41',
  }

  const testAccounts: Account[] = [
    {
      id: testAccountId,
      customerId: testCustomerId,
      createdAt: '2021-01-13T06:32:51.868Z',
      accountNumber: '81638173',
      accountName: 'Money Market Account',
      balance: '433.41',
    },
    {
      id: testAccountId + 1,
      customerId: testCustomerId,
      createdAt: '2021-01-14T06:32:51.868Z',
      accountNumber: '81638174',
      accountName: 'Money Market Account',
      balance: '231.03',
    },
  ]

  const testTransactions: Transaction[] = [
    {
      id: '1',
      accountId: testAccountId,
      date: '2020-11-21T18:22:46.716Z',
      amount: '870.92',
      transactionType: TransactionType.invoice,
    },
    {
      id: '2',
      accountId: testAccountId,
      date: '2020-11-20T18:22:46.716Z',
      amount: '70.29',
      transactionType: TransactionType.invoice,
    },
  ]

  beforeEach(
    waitForAsync(() => {
      accountService = instance<AccountService>(AccountService)
      when(accountService.getAccount(testCustomerId, testAccountId)).thenReturn(of(testAccount))
      when(accountService.getAccounts(testCustomerId)).thenReturn(of(testAccounts))

      transactionService = instance<TransactionService>(TransactionService)
      when(transactionService.getTransactions(testCustomerId, testAccountId)).thenReturn(of(testTransactions))

      sessionService = instance<AcmeSessionService>(AcmeSessionService)
      when(sessionService.getCustomerId()).thenReturn(testCustomerId)

      try {
        TestBed.configureTestingModule({
          schemas: [NO_ERRORS_SCHEMA],
          declarations: [AccountDetails],
          imports: [BrowserAnimationsModule, TranslateTestModule, RouterTestingModule],
          providers: [
            {
              provide: AccountService,
              useValue: accountService,
            },
            {
              provide: TransactionService,
              useValue: transactionService,
            },
            {
              provide: AcmeSessionService,
              useValue: sessionService,
            },
            {
              provide: ActivatedRoute,
              useValue: { params: of({ id: testAccountId }) },
            },
          ],
        }).compileComponents()
      } catch (error) {
        console.error(error)
      }
    }),
  ),
    beforeEach(() => {
      fixture = TestBed.createComponent(AccountDetails)
      component = fixture.componentInstance
      fixture.detectChanges()
    })

  it('should initialize AccountDetails with accounts$ successfully', (done) => {
    component.accounts$.subscribe((accounts) => {
      expect(accounts).toEqual(testAccounts)
      done()
    })
  })

  it('should initialize AccountDetails with transactions$ successfully', async(done) => {
    component.getTransactions(testAccountId)
    component.transactions$.subscribe((transactions) => {
      expect(transactions).toEqual(testTransactions)
      done()
    })
  })

  it('should format transaction type correctly', () => {
    expect(component.formatTransactionType(TransactionType.invoice)).toEqual(
      'transaction.type.' + TransactionType.invoice,
    )
  })
})

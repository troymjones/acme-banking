import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { inject, TestBed } from '@angular/core/testing'
import { Transaction, TransactionType } from 'src/models/transaction'
import { TransactionService } from './transaction.service'

describe('TransactionService', () => {
  let httpMock: HttpTestingController

  const testCustomerId = '111'
  const testAccountId = '222'
  const testTransactionId = '555'

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    })
    httpMock = TestBed.inject(HttpTestingController)
  })

  afterEach(() => {
    httpMock.verify()
  })

  it('should return getTransaction correctly', inject([TransactionService], (service: TransactionService) => {
    const testTransaction: Transaction = {
      id: testTransactionId,
      accountId: testAccountId,
      date: '2020-11-21T18:22:46.716Z',
      amount: '870.92',
      transactionType: TransactionType.invoice,
    }

    service.getTransaction(testCustomerId, testAccountId, testTransactionId).subscribe((transaction) => {
      expect(transaction).toEqual(testTransaction)
    })

    const req = httpMock.expectOne(
      `/api/v1/customer/${testCustomerId}/account/${testTransaction.accountId}/transaction/${testTransaction.id}`,
    )
    expect(req.request.method).toBe('GET')
    req.flush(testTransaction)
  }))

  it('should return getTransactions correctly', inject([TransactionService], (service: TransactionService) => {
    const testTransactions: Transaction[] = [
      {
        id: testTransactionId,
        accountId: testAccountId,
        date: '2020-11-21T18:22:46.716Z',
        amount: '870.92',
        transactionType: TransactionType.invoice,
      },
      {
        id: testTransactionId + 1,
        accountId: testAccountId,
        date: '2020-11-21T18:22:46.716Z',
        amount: '870.92',
        transactionType: TransactionType.invoice,
      },
    ]

    service.getTransactions(testCustomerId, testAccountId).subscribe((transactions) => {
      expect(transactions).toEqual(testTransactions)
    })

    const req = httpMock.expectOne(
      `/api/v1/customer/${testCustomerId}/account/${testTransactions[0].accountId}/transaction`,
    )
    expect(req.request.method).toBe('GET')
    req.flush(testTransactions)
  }))
})

import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Customer } from 'src/models/customer'
import { Observable } from 'rxjs'

@Injectable({ providedIn: 'root' })
export class CustomerService {
  constructor(private http: HttpClient) {}

  public getCustomer(id: string): Observable<Customer> {
    return this.http.get<Customer>(`/api/v1/customer/${id}`)
  }
}

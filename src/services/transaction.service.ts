import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Transaction } from 'src/models/transaction'
import { Observable } from 'rxjs'

@Injectable({ providedIn: 'root' })
export class TransactionService {
  constructor(private http: HttpClient) {}

  public getTransactions(customerId: string, id: string): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(`/api/v1/customer/${customerId}/account/${id}/transaction`)
  }

  public getTransaction(customerId: string, accountId: string, id: string): Observable<Transaction> {
    return this.http.get<Transaction>(`/api/v1/customer/${customerId}/account/${accountId}/transaction/${id}`)
  }
}

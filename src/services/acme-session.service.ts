import { Injectable } from '@angular/core'
import { Customer } from 'src/models/customer'

@Injectable({ providedIn: 'root' })
export class AcmeSessionService {
  private readonly customerIdKey = 'customerID'
  private readonly customerKey = 'customer'

  public getCustomerId(): string {
    return sessionStorage.getItem(this.customerIdKey) ?? '1'
  }

  public setCustomerId(id: string): void {
    sessionStorage.setItem(this.customerIdKey, id)
  }

  public getCustomer(): Customer {
    return JSON.parse(sessionStorage.getItem(this.customerKey) ?? '{}') as Customer
  }

  public setCustomer(customer: Customer) {
    sessionStorage.setItem(this.customerKey, JSON.stringify(customer))
  }
}

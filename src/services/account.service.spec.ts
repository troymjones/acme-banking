import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { inject, TestBed } from '@angular/core/testing'
import { Account } from 'src/models/account'
import { AccountService } from './account.service'

describe('AccountService', () => {
  let httpMock: HttpTestingController

  const testCustomerId = '111'
  const testAccountId = '555'

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    })
    httpMock = TestBed.inject(HttpTestingController)
  })

  afterEach(() => {
    httpMock.verify()
  })

  it('should return getAccounts correctly', inject([AccountService], (service: AccountService) => {
    const testAccounts: Account[] = [
      {
        id: testAccountId,
        customerId: testCustomerId,
        createdAt: '2021-01-13T06:32:51.868Z',
        accountNumber: '81638173',
        accountName: 'Money Market Account',
        balance: '433.41',
      },
      {
        id: testAccountId + 1,
        customerId: testCustomerId,
        createdAt: '2021-01-14T06:32:51.868Z',
        accountNumber: '81638174',
        accountName: 'Money Market Account',
        balance: '231.03',
      },
    ]

    service.getAccounts(testCustomerId).subscribe((accounts) => {
      expect(accounts).toEqual(testAccounts)
    })

    const req = httpMock.expectOne(`/api/v1/customer/${testCustomerId}/account`)
    expect(req.request.method).toBe('GET')
    req.flush(testAccounts)
  }))

  it('should return getAccount correctly', inject([AccountService], (service: AccountService) => {
    const testAccount: Account = {
      id: testAccountId,
      customerId: testCustomerId,
      createdAt: '2021-01-13T06:32:51.868Z',
      accountNumber: '81638173',
      accountName: 'Money Market Account',
      balance: '433.41',
    }

    service.getAccount(testCustomerId, testAccountId).subscribe((account) => {
      expect(account).toEqual(testAccount)
    })

    const req = httpMock.expectOne(`/api/v1/customer/${testAccount.customerId}/account/${testAccount.id}`)
    expect(req.request.method).toBe('GET')
    req.flush(testAccount)
  }))
})

import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Account } from 'src/models/account'
import { Observable } from 'rxjs'

@Injectable({ providedIn: 'root' })
export class AccountService {
  constructor(private http: HttpClient) {}

  public getAccounts(customerId: string): Observable<Account[]> {
    return this.http.get<Account[]>(`/api/v1/customer/${customerId}/account`)
  }

  public getAccount(customerId: string, id: string): Observable<Account> {
    return this.http.get<Account>(`/api/v1/customer/${customerId}/account/${id}`)
  }
}

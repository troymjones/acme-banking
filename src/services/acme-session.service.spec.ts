import { inject, TestBed } from '@angular/core/testing'
import { AcmeSessionService } from './acme-session.service'

describe('AcmeSessionService', () => {
  const testCustomerId = '555'

  let sessionStorageSetItemSpy: jest.Mock
  let getItemSpy: jest.Mock

  beforeEach(() => {
    global.sessionStorage.setItem = sessionStorageSetItemSpy

    sessionStorageSetItemSpy = jest.fn()
    global.sessionStorage.setItem = sessionStorageSetItemSpy
    getItemSpy = jest.fn()
    global.sessionStorage.getItem = getItemSpy

    TestBed.configureTestingModule({
      providers: [],
    })
  })

  it('should setCustomerId correctly', inject([AcmeSessionService], (service: AcmeSessionService) => {
    service.setCustomerId(testCustomerId)
    expect(sessionStorageSetItemSpy).toHaveBeenLastCalledWith(jasmine.any(String), testCustomerId)
  }))

  it('should getCustomerId correctly', inject([AcmeSessionService], (service: AcmeSessionService) => {
    service.getCustomerId()
    expect(getItemSpy).toHaveBeenCalledTimes(1)
  }))
})

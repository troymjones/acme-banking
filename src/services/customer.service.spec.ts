import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { inject, TestBed } from '@angular/core/testing'
import { Customer } from 'src/models/customer'
import { CustomerService } from './customer.service'

describe('CustomerService', () => {
  let httpMock: HttpTestingController

  const testCustomerId = '555'

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    })
    httpMock = TestBed.inject(HttpTestingController)
  })

  afterEach(() => {
    httpMock.verify()
  })

  it('should return getCustomer correctly', inject([CustomerService], (service: CustomerService) => {
    const testCustomer: Customer = {
      id: testCustomerId,
      avatar: 'my-avatar.png',
      createdAt: '2021-01-13T06:32:51.868Z',
      customerId: '398483',
      name: 'John Doe',
    }

    service.getCustomer(testCustomerId).subscribe((customer) => {
      expect(customer).toEqual(testCustomer)
    })

    const req = httpMock.expectOne(`/api/v1/customer/${testCustomer.id}`)
    expect(req.request.method).toBe('GET')
    req.flush(testCustomer)
  }))
})

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { AcmeBankingModule } from './acme-banking.module'
import { AcmeBankingComponent } from './acme-banking.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { BrowserModule } from '@angular/platform-browser'
import { EnvModule } from 'src/modules/env/env.module'
import { AcmeTranslateModule } from 'src/modules/translate/translate.module'
import { ACME_BANKING_ROUTES } from './routes'
import { ApiInterceptor } from 'src/interceptors/api.interceptor'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    BrowserAnimationsModule,
    BrowserModule,

    AcmeTranslateModule,

    EnvModule,
    AcmeBankingModule,
    NgbModule,
    RouterModule.forRoot(ACME_BANKING_ROUTES, {
      scrollPositionRestoration: 'top',
    }),
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true }],
  bootstrap: [AcmeBankingComponent],
})
export class MainModule {}

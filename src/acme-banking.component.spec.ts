import { ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { AcmeBankingComponent } from './acme-banking.component'
import { MockComponent } from 'ng-mocks'
import { PageHeader } from 'src/components/page-header/page-header'

describe('AcmeBankingComponent', () => {
  let component: AcmeBankingComponent
  let fixture: ComponentFixture<AcmeBankingComponent>

  beforeEach((): void => {
    TestBed.configureTestingModule({
      declarations: [AcmeBankingComponent, MockComponent(PageHeader)],
      imports: [RouterTestingModule],
      providers: [],
    }).compileComponents()
  })

  beforeEach((): void => {
    fixture = TestBed.createComponent(AcmeBankingComponent)
    component = fixture.componentInstance
  })

  it('should initialize AcmeBankingComponent successfully', (): void => {
    expect(component).toBeTruthy()
  })
})

import { Routes } from '@angular/router'

export const ACME_BANKING_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: `dashboard`,
    loadChildren: () => import('./views/dashboard/dashboard.module').then((m) => m.DashboardModule),
  },
  {
    path: `account`,
    loadChildren: () => import('./views/account-details/account-details.module').then((m) => m.AccountDetailsModule),
  },
]

export class Transaction {
  id: string
  accountId: string
  date: string
  amount: string
  transactionType: TransactionType
}

export enum TransactionType {
  deposit = 'deposit',
  invoice = 'invoice',
  payment = 'payment',
  withdrawal = 'withdrawal',
}

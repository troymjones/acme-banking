export class Customer {
  id: string
  createdAt: string
  name: string
  avatar: string
  customerId: string
}

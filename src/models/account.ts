export class Account {
  id: string
  customerId: string
  createdAt: string
  accountNumber: string
  accountName: string
  balance: string
}

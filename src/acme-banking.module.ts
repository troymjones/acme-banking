import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { AcmeBankingComponent } from './acme-banking.component'
import { ACME_BANKING_ROUTES } from './routes'
import { PageHeaderModule } from 'src/components/page-header/page-header.module'

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    PageHeaderModule,
    RouterModule.forRoot(ACME_BANKING_ROUTES, {
      scrollPositionRestoration: 'top',
    }),
  ],
  declarations: [AcmeBankingComponent],
  exports: [AcmeBankingComponent],
})
export class AcmeBankingModule {}

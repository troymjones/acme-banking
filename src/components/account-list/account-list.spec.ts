import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { of } from 'rxjs'
import { AccountService } from 'src/services/account.service'
import { RouterTestingModule } from '@angular/router/testing'
import { instance, when } from 'testdouble'
import { AccountList } from './account-list'
import { Account } from 'src/models/account'
import { AcmeSessionService } from 'src/services/acme-session.service'
import { TranslateTestModule } from 'src/modules/translate/translate.mock'

describe('AccountList', () => {
  let component: AccountList
  let fixture: ComponentFixture<AccountList>
  let accountService: AccountService
  let sessionService: AcmeSessionService

  const testCustomerId = '111'
  const testAccountId = '555'

  const testAccounts: Account[] = [
    {
      id: testAccountId,
      customerId: testCustomerId,
      createdAt: '2021-01-13T06:32:51.868Z',
      accountNumber: '81638173',
      accountName: 'Money Market Account',
      balance: '433.41',
    },
    {
      id: testAccountId + 1,
      customerId: testCustomerId,
      createdAt: '2021-01-14T06:32:51.868Z',
      accountNumber: '81638174',
      accountName: 'Money Market Account',
      balance: '231.03',
    },
  ]

  beforeEach(
    waitForAsync(() => {
      accountService = instance<AccountService>(AccountService)
      when(accountService.getAccounts(testCustomerId)).thenReturn(of(testAccounts))

      sessionService = instance<AcmeSessionService>(AcmeSessionService)
      when(sessionService.getCustomerId()).thenReturn(testCustomerId)

      try {
        TestBed.configureTestingModule({
          schemas: [NO_ERRORS_SCHEMA],
          declarations: [AccountList],
          imports: [BrowserAnimationsModule, TranslateTestModule, RouterTestingModule],
          providers: [
            {
              provide: AccountService,
              useValue: accountService,
            },
            {
              provide: AcmeSessionService,
              useValue: sessionService,
            },
          ],
        }).compileComponents()
      } catch (error) {
        console.error(error)
      }
    }),
  ),
    beforeEach(() => {
      fixture = TestBed.createComponent(AccountList)
      component = fixture.componentInstance
      fixture.detectChanges()
    })

  it('should initialize AccountList with accounts$ successfully', (done) => {
    component.accounts$.subscribe((accounts) => {
      expect(accounts).toEqual(testAccounts)
      done()
    })
  })
})

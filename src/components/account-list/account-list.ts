import { Component, Input } from '@angular/core'
import { UntilDestroy } from '@ngneat/until-destroy'
import { AccountService } from '../../services/account.service'
import { Account } from '../../models/account'
import { Observable } from 'rxjs'
import { AcmeSessionService } from 'src/services/acme-session.service'

@UntilDestroy()
@Component({
  selector: 'account-list',
  templateUrl: 'account-list.html',
  styleUrls: ['account-list.scss'],
})
export class AccountList {
  @Input() public selectedAccountId: string
  public accounts$: Observable<Account[]>

  private readonly customerId: string

  constructor(private accountService: AccountService, private session: AcmeSessionService) {
    this.customerId = this.session.getCustomerId()
  }

  public ngOnInit(): void {
    this.accounts$ = this.accountService.getAccounts(this.customerId)
  }
}

import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { RouterModule } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { AccountList } from './account-list'

@NgModule({
  imports: [CommonModule, MatProgressSpinnerModule, TranslateModule, RouterModule],
  declarations: [AccountList],
  providers: [],
  exports: [AccountList],
})
export class AccountListModule {}

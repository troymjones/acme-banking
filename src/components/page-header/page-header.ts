import { Component } from '@angular/core'
import { UntilDestroy } from '@ngneat/until-destroy'
import { AcmeSessionService } from '../../services/acme-session.service'
import { Customer } from '../../models/customer'

@UntilDestroy()
@Component({
  selector: 'page-header',
  templateUrl: 'page-header.html',
  styleUrls: ['page-header.scss'],
})
export class PageHeader {
  public customer: Customer

  constructor(private session: AcmeSessionService) {
    this.customer = this.session.getCustomer()
  }
}

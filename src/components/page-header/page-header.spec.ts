import { ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { PageHeader } from './page-header'
import { TranslateTestModule } from 'src/modules/translate/translate.mock'

describe('PageHeader', () => {
  let component: PageHeader
  let fixture: ComponentFixture<PageHeader>

  beforeEach((): void => {
    TestBed.configureTestingModule({
      declarations: [PageHeader],
      imports: [TranslateTestModule, RouterTestingModule],
      providers: [],
    }).compileComponents()
  })

  beforeEach((): void => {
    fixture = TestBed.createComponent(PageHeader)
    component = fixture.componentInstance
  })

  it('should initialize PageHeader successfully', (): void => {
    expect(component).toBeTruthy()
  })
})

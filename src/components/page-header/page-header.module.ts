import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { TranslateModule } from '@ngx-translate/core'
import { PageHeader } from './page-header'

@NgModule({
  imports: [CommonModule, TranslateModule],
  declarations: [PageHeader],
  providers: [],
  exports: [PageHeader],
})
export class PageHeaderModule {}

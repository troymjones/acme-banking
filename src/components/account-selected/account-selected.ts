import { Component, Input } from '@angular/core'
import { UntilDestroy } from '@ngneat/until-destroy'
import { Account } from '../../models/account'
import { Transaction, TransactionType } from '../../models/transaction'
import { Observable } from 'rxjs'
import { TranslateService } from '@ngx-translate/core'
import { DateTime } from 'luxon'

@UntilDestroy()
@Component({
  selector: 'account-selected',
  templateUrl: 'account-selected.html',
  styleUrls: ['account-selected.scss'],
})
export class AccountSelected {
  @Input() public account$: Observable<Account>
  @Input() public transactions$: Observable<Transaction[]>

  constructor(private translate: TranslateService) {}

  public formatTransactionType(type: TransactionType): string {
    return this.translate.instant('transaction.type.' + type)
  }

  public formatMonthDay(date: string): string {
    return DateTime.fromISO(date).toFormat('MMM d')
  }

  public formatYear(date: string): string {
    return DateTime.fromISO(date).toFormat('yyyy')
  }

  public formatMonthYear(date: string): string {
    return DateTime.fromISO(date).toFormat('MMMM yyyy')
  }
}

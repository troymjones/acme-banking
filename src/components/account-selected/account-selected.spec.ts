import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { RouterTestingModule } from '@angular/router/testing'
import { AccountSelected } from './account-selected'
import { TransactionType } from 'src/models/transaction'
import { TranslateTestModule } from 'src/modules/translate/translate.mock'

describe('AccountSelected', () => {
  let component: AccountSelected
  let fixture: ComponentFixture<AccountSelected>

  beforeEach(
    waitForAsync(() => {
      try {
        TestBed.configureTestingModule({
          schemas: [NO_ERRORS_SCHEMA],
          declarations: [AccountSelected],
          imports: [BrowserAnimationsModule, TranslateTestModule, RouterTestingModule],
          providers: [],
        }).compileComponents()
      } catch (error) {
        console.error(error)
      }
    }),
  ),
    beforeEach(() => {
      fixture = TestBed.createComponent(AccountSelected)
      component = fixture.componentInstance
      fixture.detectChanges()
    })

  it('should format transaction type correctly', () => {
    expect(component.formatTransactionType(TransactionType.invoice)).toEqual(
      'transaction.type.' + TransactionType.invoice,
    )
  })

  it('should format month/day correctly', () => {
    expect(component.formatMonthDay('2020-01-02')).toEqual('Jan 2')
    expect(component.formatMonthDay('2021-02-22')).toEqual('Feb 22')
  })

  it('should format year correctly', () => {
    expect(component.formatYear('2020-01-02')).toEqual('2020')
    expect(component.formatYear('2021-02-22')).toEqual('2021')
  })

  it('should format month/year correctly', () => {
    expect(component.formatMonthYear('2020-01-02')).toEqual('January 2020')
    expect(component.formatMonthYear('2021-02-22')).toEqual('February 2021')
  })
})

import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { RouterModule } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { AccountSelected } from './account-selected'

@NgModule({
  imports: [CommonModule, MatProgressSpinnerModule, TranslateModule, RouterModule],
  declarations: [AccountSelected],
  providers: [],
  exports: [AccountSelected],
})
export class AccountSelectedModule {}

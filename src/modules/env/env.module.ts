import { NgModule } from '@angular/core'
import * as _ from 'lodash-es'

export class EnvConfig {
  public apiBaseUrl: string
  public apiBasePath: string
}

export const EnvConfigDefault: EnvConfig = {
  apiBaseUrl: '',
  apiBasePath: '',
}

export const getEnvConfig = (browserWindow: any): EnvConfig => {
  return _.assign(EnvConfigDefault, _.get(browserWindow, '__env'))
}

export const EnvConfigProvider = {
  provide: EnvConfig,
  useFactory: (): EnvConfig => getEnvConfig(window),
}

@NgModule({ providers: [EnvConfigProvider] })
export class EnvModule {}

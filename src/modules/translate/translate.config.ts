export class AcmeTranslateConfig {
  public baseHref: string
  public prefix: string
  public suffix: string
}

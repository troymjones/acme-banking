import { HttpClient } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { TranslateModule } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
import { NgxMissingTranslationHandlerProvider, NgxTranslateLoaderProvider } from './ngx-translate.provider'
import { ACME_TRANSLATE_PROVIDERS } from './translate.provider'

const NgxTranslateConfig = {
  loader: NgxTranslateLoaderProvider,
  missingTranslationHandler: NgxMissingTranslationHandlerProvider,
  defaultLanguage: 'en',
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http)
}

@NgModule({
  exports: [TranslateModule],
  imports: [TranslateModule.forRoot(NgxTranslateConfig)],
  providers: [ACME_TRANSLATE_PROVIDERS],
})
export class AcmeTranslateModule {}

import { HttpClient } from '@angular/common/http'
import { APP_INITIALIZER } from '@angular/core'
import {
  MissingTranslationHandler,
  MissingTranslationHandlerParams,
  TranslateLoader,
  TranslateService,
} from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
import { AcmeTranslateConfig } from './translate.config'
import { NgxTranslateInitializer } from './translate.initializer'

export function NgxHttpLoaderFactory(
  http: HttpClient,
  { baseHref, prefix, suffix }: AcmeTranslateConfig,
): TranslateHttpLoader {
  const filePath = `${baseHref}${prefix}`
  return new TranslateHttpLoader(http, filePath, suffix)
}

export class NgxMissingTranslationHandler implements MissingTranslationHandler {
  public handle(params: MissingTranslationHandlerParams): string {
    console.error(`Missing translation key '${params.key}'`)
    return ''
  }
}

export const NgxTranslateInitializerProvider = {
  provide: APP_INITIALIZER,
  useFactory: NgxTranslateInitializer,
  deps: [TranslateService],
  multi: true,
}

export const NgxTranslateLoaderProvider = {
  provide: TranslateLoader,
  useFactory: NgxHttpLoaderFactory,
  deps: [HttpClient, AcmeTranslateConfig],
}

export const NgxMissingTranslationHandlerProvider = {
  provide: MissingTranslationHandler,
  useClass: NgxMissingTranslationHandler,
}

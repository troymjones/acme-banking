import { NgxTranslateInitializerProvider } from './ngx-translate.provider'
import { AcmeTranslateConfig } from './translate.config'

export const AcmeTranslateConfigDefault: AcmeTranslateConfig = {
  baseHref: './',
  prefix: 'assets/i18n/',
  suffix: '.json',
}

export const AcmeTranslateConfigProvider = {
  provide: AcmeTranslateConfig,
  useValue: AcmeTranslateConfigDefault,
}

export const ACME_TRANSLATE_PROVIDERS = [AcmeTranslateConfigProvider, NgxTranslateInitializerProvider]

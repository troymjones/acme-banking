import { TranslateService } from '@ngx-translate/core'

export function NgxTranslateInitializer(translate: TranslateService): any {
  return () => {
    translate.setDefaultLang('en')
    return translate.use('en')
  }
}

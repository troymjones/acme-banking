import { NgModule, Pipe, PipeTransform } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import _ from 'lodash-es'

export function TranslateServiceFactory(): TranslateService {
  const translateService: Partial<TranslateService> = {}
  translateService.instant = (key: string) => {
    if (_.endsWith(key, 'undefined')) {
      return ''
    }
    return key
  }
  return translateService as any
}

export const TranslateServiceTestProvider = {
  provide: TranslateService,
  useFactory: TranslateServiceFactory,
}

@Pipe({
  name: 'translate',
})
export class TranslateMockPipe implements PipeTransform {
  public name = 'translate'

  public transform(query: string, ..._args: any[]): any {
    return query
  }
}

@NgModule({
  providers: [TranslateMockPipe, TranslateServiceTestProvider],
  declarations: [TranslateMockPipe],
  exports: [TranslateMockPipe],
})
export class TranslateTestModule {}

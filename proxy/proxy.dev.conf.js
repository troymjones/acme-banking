const PROXY_CONFIG = [
  {
    context: ['/api/**'],
    target: 'https://5fff49b6a4a0dd001701bb64.mockapi.io',
    secure: false,
    changeOrigin: true,
    logLevel: 'debug',
  },
]

module.exports = PROXY_CONFIG

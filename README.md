# AcmeBanking

ACME Banking Frontend Project

**Table of Contents**

[[_TOC_]]

## AWS Deployment

http://acme-banking-dev.s3-website-us-west-2.amazonaws.com/dashboard

## Authentication Services Overview
https://gitlab.com/troymjones/acme-banking/-/blob/master/SPA%20+%20Authentication.pdf

## Authentication Sequence Diagram
https://gitlab.com/troymjones/acme-banking/-/blob/master/SPA%20+%20Auth%20Sequence%20Diagram.pdf

# Angular App

## Run

### Proxy API
`$ npm run proxy`
- Starts Angular api proxy with proxy/proxy.dev.conf.js file to proxy API requests
- Serves Angular app

### Start
`$ npm run start:env`

- Copies initial .env.example config to .env
- Replaces .env variables in env.js
- Serves Angular app with local env.js

## Test
`$ npm run test`
- Runs all jest unit tests
- Requires 80% coverage threshold to pass

## Deploy

### Branch 
- Runs `npm run build`
- Runs `npm run test`

### Merge Request
- Runs `npm run build`
- Runs `npm run test`
- Build and test jobs must pass prior to merging

### Merge
- Runs `npm run build`
- Deploys to AWS Dev S3 bucket

### Config
- ENVCONFIG_API_BASE_URL
  - Base URL for all HTTP requests 
  - Replaced in deployed env.js
- ENVCONFIG_API_BASE_PATH
  - Base Path to indicate API calls
  - Replaced in deployed env.js
- S3_*_BUCKET
  - S3 deploy bucket depending upon environment



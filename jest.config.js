module.exports = {
  preset: 'jest-preset-angular',
  collectCoverage: true,
  collectCoverageFrom: [
    '<rootDir>/src/**/*.ts',
    '!<rootDir>/src/**/index.ts',
    '!<rootDir>/src/**/main.ts',
    '!<rootDir>/src/**/polyfills.ts',
    '!<rootDir>/src/**/routes.ts',
    '!<rootDir>/src/environments/**.ts',
    '!<rootDir>/src/types/**',
    '!<rootDir>/src/**/*.module.ts',
    '!<rootDir>/src/**/*.mock.ts',
    '!<rootDir>/src/modules/**/*.ts',
    '!<rootDir>/src/**/*.model.ts',
    '!**/node_modules/**',
  ],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
  setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
  testPathIgnorePatterns: ['/node_modules/', '/dist/', '/coverage/'],
  testMatch: ['<rootDir>/src/**/*.spec.ts'],
  transformIgnorePatterns: ['node_modules/(?!(jest-test|@ngrx|lodash-es))'],
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/src/tsconfig.spec.json',
    },
  },
}
